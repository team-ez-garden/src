#include main.h
#include <string.h>
#include <stdio.h>

ADC_HandleTypeDef hadc1;
UART_HandleTypeDef huart2;

int main(void)
{
    unit16_t raw;
    char msg[50];
    float volatge;
    float moisturevalue;
    
    HAL_Init();
    
    Systemclock_Config();
    
    While (1)
    {
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
  raw = HAL_ADC_GetValue(&hadc1);
  voltage = ((float)raw)/539 * 3.3;
  moisturevalue = voltage;

  sprintf(msg, "raw value: %hu\r\n", raw);
 	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
 	 sprintf(msg, "voltage: %f\r\n", voltage);
 	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
 	 sprintf(msg, "moisture value: %f\r\n", moisturevalue);
 	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);


 	 HAL_Delay(500);
     
     return MoistureSensorHandler;
}

    }
}