typedef enum
{
    Idle_State,
    Temp_Sensor_State,
    Moisture_Sensor_State,
    Open_Valve_State,
    Close_Valve_State,
    }eSystemState;
    
ADC_HandleTypeDef hadc1;

UART_HandleTypeDef huart2;


eSystemState IdleStateHandler(void)
{
    HAL_Delay(3600*1000); // 1 hour delay
    return Idle_State;
}

eSystemState TempSensorHandler(void)
{
    // Get ADC Value from temp sensor.
    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
    raw = HAL_ADC_GetValue(&hadc1);
    
    //Convert raw value to Celsius and Farenheit.
    voltage = ((float)raw)/4096 * 3.3;
    tempC = ((voltage * 100) - 50.0);
    tempF = ((tempC * 1.8) + 32);
    
    //Convert to string and print.
    sprintf(msg, "raw value: %hu\r\n", raw);
	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
	 sprintf(msg, "voltage: %f\r\n", voltage);
	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
	 sprintf(msg, "Celsius: %f\r\n", tempC);
	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
	 sprintf(msg, "Farenheit: %f\r\n", tempF);
	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
     
     //Set 15 second delay.
     HAL_Delay(15*1000);
     
     return Temp_Sensor_State;
     }

eSystemState MoistureSensorHandler(void)
{
HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
  raw = HAL_ADC_GetValue(&hadc1);
  voltage = ((float)raw)/539 * 3.3;
  moisturevalue = voltage;

  sprintf(msg, "raw value: %hu\r\n", raw);
 	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
 	 sprintf(msg, "voltage: %f\r\n", voltage);
 	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
 	 sprintf(msg, "moisture value: %f\r\n", moisturevalue);
 	 HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);


 	 HAL_Delay(500);
     
     return MoistureSensorHandler;
}

eSystemState OpenValvehandler(void)
{
Scott insert Code here
}

eSystemState CloseValveHandler(void)
{
Scott insert code here
}
int main(void)
{

    uint16_t raw;
    char msg[128];
    float voltage;
    float tempC;
    float tempF;

    eSystemState eNextState = Idle_State;

    while(1)
    {
        switch(eNextState)
            {
        case Idle_State:
            eNextState = TempSensorHandler();
            break;
        case Temp_Sensor_State:
            eNextState = MoistureSensorHandler();
            break;
        case Moisture_Sensor_State:
            if (moisturevalue < 1.0){
            eNextState = OpenValveHandler();
            }
            else{
            eNextState = IdleStateHandler();
            }
            break;
        case Open_Valve_State:
            eNextState = CloseValveHandler();
            break;
        case Close_Valve_State:
            eNextState = IdleStateHandler();
            break;
        default:
            eNextState = IdleStateHandler();
            break;
            }
        
    }
}